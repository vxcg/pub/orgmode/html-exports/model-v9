link:
	(ln -sf ${src-dir}/figs; ln -sf ${src-dir}/bib)

link-rec: link
	@- $(foreach dir,$(subdirs), \
			(cd $(dir) && make -k link-rec);)

add-biblio:
	(grep "Bibliography" main.org > /dev/null; \
	if [ $$? -eq 0 ];  then \
		echo "Present" > ans; \
	else \
		echo "Absent" > ans; \
		cat main.org ${biblio.org} > tmp.org; \
		mv tmp.org main.org; \
		\rm -rf tmp.org; \
	fi)

add-biblio-rec: add-biblio
	@- $(foreach dir,$(subdirs), \
			(cd $(dir) && make -k add-biblio-rec);)


clean:
	(\rm -f ${main}.aux ${main}.bbl ${main}.blg ${main}.fdb_latexmk \
	${main}.fls ${main}.log ${main}.out ${main}.pdf \
	${main}.tex ${main}.toc ${main}.html)


clean-rec: clean
	@- $(foreach dir,$(subdirs), \
			(cd $(dir) && make -k clean-rec);)




