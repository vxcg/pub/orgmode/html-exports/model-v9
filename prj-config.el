(require 'org)
(require 'ob-tangle)
(set-language-environment "UTF-8")
(setq org-export-babel-evaluate nil)
(setq org-export-allow-BIND t)
(add-hook 'org-publish-before-export-hook 'org-babel-tangle 'append)
(setq org-babel-default-header-args
      (append '((:eval . "no") (:mkdirp . "yes"))
	    ;;; if mkdirp is already there, delete it
	    ;;; if eval is already there, delete it
	    ;;; if org-src-preserve-indentation is already there, delete it
	    (progn
	      (assq-delete-all :mkdirp  org-babel-default-header-args)
	      (assq-delete-all :eval    org-babel-default-header-args))))

;;; https://groups.google.com/forum/#!topic/comp.emacs/iiYJL04M7lA
;;; eliminate annoying messages from emacs about following
;;; version controlled files that are symlinks.
(setq vc-follow-symlinks t)

;;; preserve  tabs and indentation (e.g., for Python programs)
(setq org-src-preserve-indentation t)

;; Set to true to use regular expressions to expand noweb references.
;; This results in much faster noweb reference expansion but does
;; not properly allow code blocks to inherit the \":noweb-ref\"
;; header argument from buffer or subtree wide properties.")
(setq org-babel-use-quick-and-dirty-noweb-expansion t)

;;; https://stackoverflow.com/questions/698562/disabling-underscore-to-subscript-in-emacs-org-mode-export
;;; subscript and superscripts are suppressed
(setq org-export-with-sub-superscripts nil)

;;; translates CUSTOM_ID in PROPERTIES to \label in latex
(setq org-html-prefer-user-labels t)
;;; translates CUSTOM_ID in PROPERTIES to \label in latex
(setq org-latex-prefer-user-labels t)

;; (setq org-latex-listings 'minted)
;; (setq org-latex-minted-options '(
;;                                  ("frame" "lines")
;;                                  ("fontsize" "\\scriptsize")
;;                                  ("xleftmargin" "\\parindent")
;;                                  ("linenos" "")
;;                                  ))


(setq org-html-postamble t)
(setq org-html-postamble-format
  '(("en" "<p class=\"author\">%a</p>
           <p class=\"date\">%T</p>
           <p class=\"license\"><a href=\"https://creativecommons.org/licenses/by-nc-nd/4.0/\">CC-BY-NC-ND 4.0</a></p>         
           ")))



;;; themes to be excluded from publishing.
;;; It is separately copied via make -k themes
(defvar exclude-rx (rx "themes"))





;;; Experimental! Doesn't work
;;; ===========================

;;; The convention is that a normal exportable org file
;;; does NOT have a bibliography information per se.
;;; Before exporting the contents of the biblio-org-file are
;;; appended to the buffer containing the org file to be exported.

(require 'ox-bibtex)

;;; biblio-org-file contains specification of bibliography files
(defvar biblio-org-file
  (expand-file-name "~/tmp/org-infra/models/model-v9/src/biblio.org"))

(defun add-biblio-org-file (backend)
  "Hook that adds contents of biblio spec file to org-buffer
   before export"
  (message "appending biblio file to  %s" (buffer-name (current-buffer)))
  (append-file-to-buffer biblio-org-file (current-buffer)))

;; doesn't work!
;; (add-hook 'org-export-before-processing-hook #'add-biblio-org-file)
;;; -------------------------------------------------------------------

;;; This is run before the org file is
;;; preprocessed for export or publishing.

;;; Assumes that bibliography sections are commented.
;;; Uncomments them temporarily when publishing or exporting.

;;; see tutorial on rx
;;; https://francismurillo.github.io/2017-03-30-Exploring-Emacs-rx-Macro/
(defun uncomment-bibliography (backend)
  (when t
;	  (eq backend 'latex)
	(let ((pat
		   (rx bol
			   "*" (1+ white)
			   "COMMENT" (1+ white)
			   "Bibliography" (* white) eol)))
	  (while (re-search-forward pat nil t)
		(replace-match "* Bibliography")))))

;;; comment the next line to suppress this auto-uncomment feature
(add-hook 'org-export-before-processing-hook #'uncomment-bibliography)

(provide 'prj-config)

