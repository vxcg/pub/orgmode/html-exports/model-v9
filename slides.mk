# the next three targets have to do with org-reveal slides
# untested.  Code almost certainly does not work.
toc-progress:
	(mkdir -p ${src-dir}/plugin; cd ${src-dir}/plugin; \
    ln -sf ../themes/org-reveal/Reveal.js-TOC-Progress/plugin/toc-progress)

reveal: toc-progress themes
	(${emacs} -q --script ${exp-dir}/elisp/publish-cli-proxy.el \
	"${emacs-utils-dir}:${exp-dir}/elisp" ${prj-dir} org-reveal)

# assumes decktape is installed
decktape:
	@echo "conv to pdf using decktape"
	${decktape} -s 1280x760 reveal src/index.html src/index.pdf

