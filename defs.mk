SHELL:=/bin/bash
home=$(shell echo $$HOME)

# CUSTOMIZABLE
# ============

org-infra-dir=${home}/tmp/org-infra
# themes-dir=${org-infra-dir}/themes
themes-dir=${org-infra-dir}/themes
exporter-dir=${org-infra-dir}/exporters/org-v9
utils-dir=${org-infra-dir}/utils
emacs-utils-dir=${utils-dir}/emacs-utils

org-infra-path=${emacs-utils-dir}:${exporter-dir}/elisp
org-mode-path=${org-mode-dir}/lisp:${org-contrib-dir}/lisp

# prj-dir=$(shell pwd -P)
prj-dir=${home}/tmp/org-infra/models/model-v9
build-dir=${prj-dir}/build

build-docs-dir=${build-dir}/docs
src-dir=${prj-dir}/src
# file containing the bibliographic info
biblio.org= ${src-dir}/biblio.org

# https://www.gnu.org/savannah-checkouts/gnu/emacs/emacs.html#Releases
emacs=${home}/apps/emacs-27.2/src/emacs

# git clone https://git.savannah.gnu.org/git/emacs/org-mode.git
org-mode-dir=${home}/apps/org-mode-releases/org-mode-git
# git clone https://git.sr.ht/~bzg/org-contrib
org-contrib-dir=${home}/apps/org-mode-releases/org-contrib-git


lisp-path=.:${emacs-utils-dir}:${exporter-dir}/elisp:${prj-dir}:${org-mode-dir}/lisp:${org-contrib-dir}/lisp


#force = 0 will ignore already published files
# force=1  will republish all files
force=0
# output = html | latex
# generates html or latex
# default
output="html"


