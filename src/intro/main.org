#+title:  Intro to Programs
#+author: vxc

#+INCLUDE: ../themes/math/org-templates/level-1.org
#+SETUPFILE: ../themes/algo/org-templates/level-1.org
#+SETUPFILE: ../themes/readtheorg/org-templates/level-1.org
# #+LateX_CLASS: book
# ------------8------------
# ------------9------------



* Introduction
This is the introduction section.  The transition relation
is $x\xto{u}{S}x'$.

* Code

#+BEGIN_SRC python :tangle foo.py
x = 5
#+END_SRC

* Structure of Programs

Abelson and Sussman cite:abelson explore the central role of
interpreters in the ``procedural epistemology'' of
computation.

* COMMENT Bibliography
#+include: ../../biblio.org

* Biblio                                                           :noexport:
bibliography:./bib/ref.bib
bibliographystyle:plain



