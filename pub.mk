all: pub

init:
	(cd ${prj-dir}/src; ln -sf ${themes-dir};\
	mkdir -p ${build-dir}/docs/themes)

themes: init
	(rsync -r --copy-links ${themes-dir}/math ${build-dir}/docs/themes; \
	rsync  -r --copy-links ${themes-dir}/readtheorg ${build-dir}/docs/themes; \
	rsync  -r --copy-links ${themes-dir}/algo ${build-dir}/docs/themes)

pub: 
	(${emacs} -q --script ${exporter-dir}/elisp/cli.el \
	${emacs-utils-dir}/emacs-funs.el path=${lisp-path} \
	prj-dir=${prj-dir} op=pub force=${force}; echo "DONE")

# this target must run from a subdir of ${prj-dir}/src

pub-local:
	(make -k src-dir=$(shell pwd -P) pub)

publish: themes
	(make -k pub)


clean-build:
	(\rm -rf ${prj-dir}/build)
